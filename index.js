const express = require("express");
const moongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const morgan = require("morgan");
const { AuthRouter } = require("./src/routes(controller)/Auth_Router");
const { UsersRouter } = require("./src/routes(controller)/Users_Router");
const { TruckRouter } = require("./src/routes(controller)/Truck_Router");
const { LoadRouter } = require("./src/routes(controller)/Load_Router");
const { DriverRouter } = require("./src/routes(controller)/Load(driver_Router");
const { JointRouter } = require("./src/routes(controller)/Load(sh&dr)_Router");
const http_errors = require("http-errors");

const app = express();
app.use(morgan("common"));
app.use(express.json());

app.use("/api/auth", AuthRouter);
app.use("/api/users", UsersRouter);
app.use("/api/loads/active", DriverRouter);
app.use("/api/loads", JointRouter);
app.use("/api/loads", LoadRouter);
app.use("/api/trucks", TruckRouter);
moongoose
  .connect(
    "mongodb+srv://Sanya:456jkl89@cluster0.kggex.mongodb.net/notes&users?retryWrites=true&w=majority",
    {
      useUnifiedTopology: true,
      useCreateIndex: true,
      useNewUrlParser: true,
      useFindAndModify: false,
    }
  )
  .then(
    () => {
      console.log("Connected to db");
      // console.log(
      //   jwt.verify(
      //     "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6IlJhbGVpZ2guUGZhbm5lcnN0aWxsOTBAaG90bWFpbC5jb20iLCJyb2xlIjoiU0hJUFBFUiIsIl9pZCI6IjYwZjc4MjZjMzdjZGIyMWVhMjY0NGY2OSIsImlhdCI6MTYyNjgzMzUxNn0.OJtjAD-LccXwnhg9sTrpnqbF7_nQ_VB6vPvftGBvG-8",
      //     "key"
      //   )
      // );
      app.listen(8080);
    },
    () => {
      console.log("Error connect to MongoDb");
    }
  );

app.use((req, res, next) => {
  next(http_errors(400, "Invalid path"));
});
app.use((err, req, res, next) => {
  res.status(err.status || 500).json({ message: err.message });
});
