const http_errors = require("http-errors");

const isShipperMiddleware = (req, res, next) => {
  const { role } = req.user;
  console.log(`role:${role}`);
  if (role === "SHIPPER") {
    console.log("Shipper validation passed");
    next();
  } else {
    throw new http_errors(
      400,
      `Permission denied(Driver not allow to use shipper api)`
    );
  }
};

module.exports = {
  isShipperMiddleware,
};
