const jwt = require("jsonwebtoken");

const authMiddleware = (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) {
    return res.status(401).json({ message: "Provide auth header" });
  }
  const [, token] = authorization.split(" ");
  if (!token) {
    return res.status(401).json({ message: "Pls include token" });
  }
  try {
    const tokenPayload = jwt.verify(token, "key");
    req.user = {
      _id: tokenPayload._id,
      role: tokenPayload.role,
      email: tokenPayload.email,
    };
    console.log(req.user, 123);
    req.token = token;
    next();
  } catch (err) {
    console.log(err.messager);
    res.status(401).json({ message: err.message });
  }
};
module.exports = {
  authMiddleware,
};
