const http_errors = require("http-errors");

const isDriverMiddleware = (req, res, next) => {
  const { role } = req.user;
  console.log(`role:${role}`, 123);
  if (role === "DRIVER") {
    console.log("Driver validation passed");
    next();
  } else {
    throw new http_errors(
      400,
      `Permission denied(Shipper not allow to use driver api)`
    );
  }
};

module.exports = {
  isDriverMiddleware,
};
