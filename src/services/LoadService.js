const { LoadModel } = require("../models/LoadModel");
const { TruckModel } = require("../models/TruckModel");
const http_errors = require("http-errors");

const isProper = (length, width, height, type, payload) => {
  let dimensions = [
    {
      type: "SPRINTER",
      dimension: {
        currentLength: 300,
        currentWidth: 250,
        currentHeight: 170,
      },
      payload: 1700,
    },
    {
      type: "SMALL STRAIGHT",
      dimension: {
        currentLength: 500,
        currentWidth: 250,
        currentHeight: 170,
      },
      payload: 2500,
    },
    {
      type: "LARGE STRAIGHT",
      dimension: {
        currentLength: 700,
        currentWidth: 350,
        currentHeight: 200,
      },
      payload: 4000,
    },
  ];
  let currentTruck = dimensions.find((el) => {
    if (el.type.toLowerCase() === type.toLowerCase()) {
      return true;
    }
    return false;
  });
  let { currentHeight, currentWidth, currentLength } = currentTruck.dimension;
  console.log(payload, currentTruck.payload);
  if (
    ((currentLength > length &&
      currentWidth > width &&
      currentHeight > height) ||
      (currentLength > length &&
        currentWidth > height &&
        currentHeight > width)) &&
    currentTruck.payload > payload
  ) {
    return true;
  } else {
    return false;
  }
};
const addLoad = (
  userId,
  name,
  payload,
  pickup_address,
  delivery_address,
  dimensions,
  logs
) => {
  let load = new LoadModel({
    created_by: userId,
    name: name,
    payload: payload,
    pickup_address: pickup_address,
    delivery_address: delivery_address,
    dimensions: dimensions,
  });
  return load.save();
};
const findDriver = (userId, loadId) => {
  return LoadModel.findById(loadId).then((load) => {
    if (load.status === "NEW") {
      load.status = "POSTED";
      return load.save().then(() => {
        return TruckModel.find({ assigned_to: { $not: { $eq: null } } }).then(
          (trucks) => {
            let properTruck = trucks.find((el) => {
              const { length, width, height } = load.dimensions;
              let payload = load.payload;
              return isProper(length, width, height, el.type, payload);
            });
            if (properTruck) {
              properTruck.status = "OL";
              console.log(properTruck);
              return properTruck
                .save()
                .then(() => {
                  load.status = "ASSIGNED";
                  load.state = "En route to Pick Up";
                  load.assigned_to = properTruck.assigned_to;
                  return load.save();
                })
                .catch(() => {
                  load.status = "NEW";
                  return load.save();
                });
            } else {
              load.status = "NEW";
              return load.save();
            }
          }
        );
      });
    } else {
      throw new http_errors(400, `Only new loads can find driver`);
    }
  });
};
const iterateNextState = (userId) => {
  console.log(userId);
  return TruckModel.findOne({ assigned_to: userId, status: "OL" }).then(
    (truck) => {
      if (truck) {
        let states = [
          "En route to Pick Up",
          "Arrived to Pick Up",
          "En route to delivery",
          "Arrived to delivery",
        ];
        return LoadModel.findOne({ assigned_to: userId }).then(async (load) => {
          if (states[0] === load.state) {
            load.state = states[1];
            console.log(load);
            return load.save();
          } else if (states[1] === load.state) {
            load.state = states[2];
            return load.save();
          } else if (states[2] === load.state) {
            load.state = states[3];
            truck.status = "IS";
            load.status = "SHIPPED";
            // load.logs[0].message = `Assigned to driver ${truck.assigned_to}`;
            // load.logs[0].time = Date.now();
            return truck.save().then(() => {
              return load.save();
            });
          }
        });
      } else {
        throw new http_errors(400, `${load}`);
      }
    }
  );
};
const getLoadById = (loadId) => {
  return LoadModel.findOne({ _id: loadId }).then((load) => {
    if (load) {
      return load;
    } else {
      throw new http_errors(400, "No load with such id");
    }
  });
};
const isLoadNew = (loadId) => {
  return LoadModel.findById(loadId).then((load) => {
    if (load.status === "NEW") {
      return load;
    } else {
      return false;
    }
  });
};
const updateLoadInfo = (
  name,
  payload,
  pickup_address,
  delivery_address,
  dimensions,
  loadId
) => {
  return isLoadNew(loadId).then((load) => {
    if (load) {
      console.log(load);
      load.name = name;
      load.payload = payload;
      load.pickup_address = pickup_address;
      load.dimensions = dimensions;
      load.delivery_address = delivery_address;
      // console.log(load);
      return load.save();
    } else {
      throw new http_errors(400, "Permission for update is denied");
    }
  });
};
const deleteLoadById = (loadId) => {
  return LoadModel.findOne({ _id: loadId }).then((load) => {
    if (load) {
      if (load.status === "NEW") {
        return load.deleteOne();
      } else {
        throw new http_errors(400, `This load in progress.Operation denied.`);
      }
    } else {
      throw new http_errors(400, `There is no such load:${loadId}`);
    }
  });
};
const getActiveLoad = (userId) => {
  //$or: [{ status: "ASSIGNED" }, { status: "SHIPPED" }
  return LoadModel.findOne({}).then((load) => {
    if (load) {
      return load;
    } else {
      throw new http_errors(
        400,
        `There is no active load for driver with id:${userId}`
      );
    }
  });
};
const getActiveLoadById = (loadId) => {
  return LoadModel.findOne({ _id: loadId }).then((load) => {
    if (load.status !== "ASSIGNED") {
      throw new http_errors(
        400,
        `There is no active load for shipper with load id:${loadId}`
      );
    } else {
      return load;
    }
  });
};
const findAllDriverLoads = (driverId) => {
  return LoadModel.find({ assigned_to: driverId }).then((loads) => {
    return loads;
  });
};
const findAllShipperLoads = (shipperId) => {
  console.log(typeof shipperId);
  return LoadModel.find({ created_by: shipperId }).then((loads) => {
    console.log(loads);
    return loads;
  });
};
module.exports = {
  addLoad,
  findDriver,
  iterateNextState,
  getLoadById,
  updateLoadInfo,
  deleteLoadById,
  getActiveLoad,
  getActiveLoadById,
  findAllDriverLoads,
  findAllShipperLoads,
};
