const { UserModel } = require("../models/UserModel");
const { NotesModel } = require("../models/TruckModel");
const bcrypt = require("bcrypt");
const http_errors = require("http-errors");

const getUserInfo = (userId) => {
  return UserModel.findOne({ _id: userId }).then(
    (user) => {
      console.log(user);
      return user;
    },
    () => {
      throw new http_errors(400, `There is no such user with id:${userId}`);
    }
  );
};
const deleteUserbyId = (userId) => {
  return UserModel.findOne({ _id: userId }).then((user) => {
    if (user) {
      return UserModel.deleteOne({ _id: userId }).then(() => {
        console.log(user._id, NotesModel);
        return NotesModel.deleteMany({ userId: user._id }).then(() => {
          return user;
        });
      });
    }
    throw new http_errors(400, `There is no such user with id:${userId}`);
  });
};
const changePassword = (userId, oldPassword, newPassword) => {
  return UserModel.findOne({ _id: userId }).then((user) => {
    if (!user) {
      throw new http_errors(400, `There is no such user with id:${userId}`);
    }
    return bcrypt.compare(oldPassword, user.password).then((result) => {
      if (result) {
        return bcrypt.hash(newPassword, 10).then(
          (hashPassword) => {
            return user.updateOne({ password: hashPassword });
          },
          () => {
            throw new Error(`Something went wrong with hash`);
          }
        );
      } else {
        throw new Error("Wrong old password");
      }
    });
  });
};
module.exports = { getUserInfo, deleteUserbyId, changePassword };
