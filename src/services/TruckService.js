const { TruckModel } = require("../models/TruckModel");
const http_errors = require("http-errors");

const getTrucks = (userId) => {
  return TruckModel.find({ createdBy: userId });
};
const AddTruck = (userId, truckType) => {
  console.log(userId, truckType);
  let truck = new TruckModel({
    createdBy: userId,
    type: truckType,
  });
  return truck.save();
};
const getTruckById = (userId, truckId) => {
  console.log(userId, truckId);
  return TruckModel.find({ _id: truckId, createdBy: userId }).then((truck) => {
    if (truck) {
      return truck;
    }
    throw new http_errors(400, `There is no such truck with id:${truckId}`);
  });
};
const updateTruckInfo = (userId, truckId, newType) => {
  let typeArr = ["SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT", null];
  let typeValidation = false;
  typeArr.forEach((el) => {
    if (newType === el) {
      typeValidation = true;
    }
  });
  if (typeValidation) {
    if (truck.status === "IS") {
      return TruckModel.findOneAndUpdate(
        { _id: truckId, createdBy: userId },
        { $set: { type: newType } }
      ).then((truck) => {
        if (truck) {
          console.log(truck);
        } else {
          throw new http_errors(
            400,
            `There is no such truck with id:${truckId}`
          );
        }
      });
    } else {
      throw new http_errors(400, `Can not update OL trucks`);
    }
  } else {
    throw new http_errors(400, `Invalid new Type`);
  }
};
const deleteTruckById = (userId, truckId) => {
  return TruckModel.findOne({ _id: truckId, createdBy: userId }).then(
    (truck) => {
      if (truck) {
        if (truck.status === "IS") {
          return truck.deleteOne({});
        }
        throw new http_errors(400, `Can not update OL trucks`);
      }
      throw new http_errors(400, `There is no such truck with id:${truckId}`);
    }
  );
};
const isDriverAssigned = (userId) => {
  return TruckModel.findOne({
    assigned_to: {
      $not: { $eq: null },
    },
    createdBy: userId,
  }).then((truck) => {
    return truck;
  });
};
const assignTruck = (userId, truckId) => {
  return isDriverAssigned(userId).then((assignedTruck) => {
    return TruckModel.findOne({ _id: truckId }).then((truck) => {
      if (truck) {
        if (!assignedTruck) {
          console.log(typeof truckId);
          return TruckModel.findOne({ _id: truckId }).then((truck) => {
            console.log(truck.createdBy, userId);
            if (truck.createdBy === userId) {
              truck.assigned_to = userId;
              return truck.save();
            } else {
              throw new http_errors(400, "Permission denied");
            }
          });
        } else if (assignedTruck.status === "OL") {
          throw new http_errors(400, "Driver already busy(status OL)");
        } else if (
          assignedTruck._id.toString() === truckId.toString() &&
          assignedTruck.status !== "OL"
        ) {
          truck.assigned_to = null;
          return truck.save();
        } else {
          throw new http_errors(400, "Driver already assigned");
        }
      } else {
        throw new http_errors(400, `No suck truck with id:${truckId}`);
      }
    });
  });
};
module.exports = {
  getTrucks,
  AddTruck,
  getTruckById,
  updateTruckInfo,
  deleteTruckById,
  assignTruck,
};
