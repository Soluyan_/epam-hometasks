const express = require("express");
const UsersRouter = express.Router();
const http_errors = require("http-errors");
// const jwt = require("jsonwebtoken");
const { authMiddleware } = require("../middlewares/Auth_middleware");
const {
  getUserInfo,
  deleteUserbyId,
  changePassword,
} = require("../services/UserService");

UsersRouter.use(authMiddleware);
UsersRouter.get("/me", (req, res) => {
  getUserInfo(req.user._id)
    .then((user) => {
      let { createdDate, _id, username } = user;
      res.status(200).json({
        user: {
          _id: _id,
          username: username,
          createdDate: createdDate,
        },
      });
    })
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
});
UsersRouter.delete("/me", (req, res) => {
  deleteUserbyId(req.user._id).then((user) => {
    // jwt.destroy(req.token);
    console.log(user.username);
    res.status(200).json({ message: `user:${user._id} was deleted` });
  });
});
UsersRouter.patch("/me", (req, res, next) => {
  let { oldPassword, newPassword } = req.body;
  let { _id } = req.user;
  changePassword(_id, oldPassword, newPassword)
    .then(() => {
      res.status(200).json({ message: `Password was successfully changed` });
    })
    .catch((err) => {
      next(http_errors(400, err.message));
    });
});
module.exports = {
  UsersRouter,
};
