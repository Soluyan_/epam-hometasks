const express = require("express");
const {
  findAllShipperLoads,
  findAllDriverLoads,
  getLoadById,
} = require("../services/LoadService");
const JointRouter = express.Router();

const { authMiddleware } = require("../middlewares/Auth_middleware");
JointRouter.use(authMiddleware);

JointRouter.get("/", (req, res) => {
  let { role, _id } = req.user;
  if (role === "DRIVER") {
    let driverId = _id;
    findAllDriverLoads(driverId)
      .then((loads) => {
        res.status(200).json({ loads: loads });
      })
      .catch((err) => {
        res.status(400).json({ message: err.message });
      });
  } else if (role === "SHIPPER") {
    let shipperId = _id;
    findAllShipperLoads(shipperId)
      .then((loads) => {
        res.status(200).json({ loads: loads });
      })
      .catch((err) => {
        res.status(400).json({ message: err.message });
      });
  }
});
JointRouter.get("/:id", (req, res, next) => {
  const loadId = req.params.id;
  getLoadById(loadId)
    .then((load) => {
      res.status(200).json({ load: load });
    })
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
});
module.exports = {
  JointRouter,
};
