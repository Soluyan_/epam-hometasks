const express = require("express");
const {
  addLoad,
  findDriver,
  getLoadById,
  updateLoadInfo,
  getActiveLoadById,
  deleteLoadById,
} = require("../services/LoadService");

const LoadRouter = express.Router();
const { authMiddleware } = require("../middlewares/Auth_middleware");
const { isShipperMiddleware } = require("../middlewares/isShipper_middleware");
const { LoadModel } = require("../models/LoadModel");

LoadRouter.use(authMiddleware);
LoadRouter.use(isShipperMiddleware);

LoadRouter.get("/:id/shipping_info", (req, res, next) => {
  const { _id } = req.user;
  const loadId = req.params.id;
  getActiveLoadById(loadId)
    .then((load) => {
      res.status(200).json({ load: load });
    })
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
});
LoadRouter.post("/", (req, res, next) => {
  const { _id } = req.user;
  const { name, payload, pickup_address, delivery_address, dimensions } =
    req.body;
  if (name && payload && pickup_address && delivery_address && dimensions) {
    addLoad(_id, name, payload, pickup_address, delivery_address, dimensions)
      .then(() => {
        res.status(200).json({ message: "Load created successfully" });
      })
      .catch((err) => {
        res.status(400).json({ message: err.message });
      });
  } else {
    res.status(400).json({ message: "Specify all body-arguments" });
  }
});
LoadRouter.post("/:id/post", (req, res, next) => {
  const userId = req.user._id;
  const loadId = req.params.id;
  console.log(userId, loadId);
  findDriver(userId, loadId)
    .then(() => {
      res.status(200).json({
        message: "Load posted successfully",
        driver_found: true,
      });
    })
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
});
LoadRouter.put("/:id", (req, res, next) => {
  let { name, payload, pickup_address, delivery_address, dimensions } =
    req.body;
  let { width, height, length } = dimensions;
  let loadId = req.params.id;
  if (
    name &&
    payload &&
    pickup_address &&
    delivery_address &&
    height &&
    width &&
    length &&
    dimensions
  ) {
    updateLoadInfo(
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
      loadId
    )
      .then((load) => {
        res.status(200).json({
          message: "Load details changed successfully",
        });
      })
      .catch((err) => {
        res.status(400).json({ message: err.message });
      });
  } else {
    res.status(400).json({ message: "Pls specify all body params" });
  }
});
LoadRouter.delete("/:id", (req, res) => {
  let loadId = req.params.id;
  deleteLoadById(loadId)
    .then((load) => {
      res.status(200).json({
        message: "Load deleted successfully",
      });
    })
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
});
module.exports = {
  LoadRouter,
};
