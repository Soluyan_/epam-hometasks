const express = require("express");

const {
  getTrucks,
  AddTruck,
  getTruckById,
  updateTruckInfo,
  deleteTruckById,
  assignTruck,
} = require("../services/TruckService");

const TruckRouter = express.Router();
const { authMiddleware } = require("../middlewares/Auth_middleware");
const { isDriverMiddleware } = require("../middlewares/isDriver_middleware");

TruckRouter.use(authMiddleware);
TruckRouter.use(isDriverMiddleware);

TruckRouter.get("/", (req, res) => {
  const { _id } = req.user;
  getTrucks(_id)
    .then((trucks) => {
      if (trucks) {
        res.status(200).json({ trucks: trucks });
      }
    })
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
});
TruckRouter.post("/", (req, res) => {
  const { _id } = req.user;
  const { type } = req.body;
  if (!type) {
    return res.status(400).json({ message: "Pls specify type" });
  }
  AddTruck(_id, type)
    .then(() => {
      res.status(200).json({ message: "Truck created successfully" });
    })
    .catch((err) => {
      res.status(400).json({ message: "Something went wrong" });
    });
});

TruckRouter.get("/:id", (req, res) => {
  const userId = req.user._id;
  const truckId = req.params.id;
  getTruckById(userId, truckId)
    .then((truck) => {
      res.status(200).json({ truck: truck });
    })
    .catch((err) => {
      res.status(400).json({ message: "Something went wrong" });
    });
});

TruckRouter.put("/:id", (req, res) => {
  const userId = req.user._id;
  const truckId = req.params.id;
  const { type } = req.body;
  updateTruckInfo(userId, truckId, type)
    .then(() => {
      res.status(200).json({ message: "Truck details changed successfully" });
    })
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
});
TruckRouter.delete("/:id", (req, res) => {
  const userId = req.user._id;
  const truckId = req.params.id;
  deleteTruckById(userId, truckId)
    .then(() => {
      res.status(200).json({ message: "Truck deleted successfully" });
    })
    .catch((err) => {
      res.status(400).json({ message: "Something went wrong" });
    });
});
TruckRouter.post("/:id/assign", (req, res) => {
  const userId = req.user._id;
  const truckId = req.params.id;
  // console.log(userId, truckId);
  assignTruck(userId, truckId)
    .then(() => {
      res.status(200).json({ message: "Truck assigned successfully" });
    })
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
});
module.exports = {
  TruckRouter,
};
