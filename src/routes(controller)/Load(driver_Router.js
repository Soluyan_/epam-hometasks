const express = require("express");
const {
  addLoad,
  findDriver,
  iterateNextState,
  getLoadById,
  getActiveLoad,
  updateLoadInfo,
} = require("../services/LoadService");
const DriverRouter = express.Router();

const { authMiddleware } = require("../middlewares/Auth_middleware");
const { isDriverMiddleware } = require("../middlewares/isDriver_middleware");

DriverRouter.use(authMiddleware);
DriverRouter.use(isDriverMiddleware);

DriverRouter.get("/", (req, res) => {
  let userId = req.user._id;
  getActiveLoad(userId)
    .then((load) => {
      res.status(200).json({ load: load });
    })
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
});
DriverRouter.patch("/state", (req, res) => {
  const userId = req.user._id;
  console.log(userId);
  iterateNextState(userId)
    .then((load) => {
      res.status(200).json({
        message: `Load state changed to ${load.state}`,
      });
    })
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
});
module.exports = {
  DriverRouter,
};
