const express = require("express");
const AuthRouter = express.Router();
const {
  AuthService,
  signIn,
  forgotPassword,
} = require("../services/AuthService");
const http_errors = require("http-errors");

AuthRouter.post("/register", async (req, res) => {
  let { email, password, role } = req.body;
  AuthService(email, password, role)
    .then(() => {
      console.log("User created successfully");
      res.status(200).json({ message: "User created successfully" });
    })
    .catch((err) => {
      console.log("User was not created.Something went wrong");
      res.status(400).json({ message: err.message });
    });
});
AuthRouter.post("/login", (req, res, next) => {
  let { email, password } = req.body;
  signIn(email, password, next)
    .then((token) => {
      console.log(token, 123);
      res.json({ jwt_token: token });
    })
    .catch((err) => {
      next(http_errors(400, err.message));
    });
});
AuthRouter.post("/forgot_password", (req, res, next) => {
  let { email } = req.body;
  forgotPassword(email).then((user) => {
    if (user) {
      res.json({
        message: "New password sent to your email address",
      });
    } else {
      next(http_errors(400, `User with such email:${email} does not exist`));
    }
  });
});

module.exports = { AuthRouter };
