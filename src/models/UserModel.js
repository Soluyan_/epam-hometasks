const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
  role: {
    type: String,
    enum: ["DRIVER", "SHIPPER"],
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: false,
    unique: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});
const UserModel = mongoose.model("users", UserSchema);
module.exports = { UserModel };
