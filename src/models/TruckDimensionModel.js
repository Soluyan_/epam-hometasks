const mongoose = require("mongoose");

const DimensionSchema = new mongoose.Schema({
  type: {
    type: String,
    required: true,
  },
  dimensions: {
    length: {
      type: String,
      required: true,
    },
    width: {
      type: String,
      required: true,
    },
    height: {
      type: String,
      required: true,
    },
  },
});
const DimensionModel = mongoose.model("trucks_dimensions", DimensionSchema);
module.exports = { DimensionModel };
