const mongoose = require("mongoose");

const LoadSchema = new mongoose.Schema({
  assigned_to: {
    type: String,
    required: false,
    default: null,
  },
  status: {
    type: String,
    required: false,
    enum: ["NEW", "POSTED", "ASSIGNED", "SHIPPED"],
    default: "NEW",
  },
  state: {
    type: String,
    required: false,
    enum: [
      "En route to Pick Up",
      "Arrived to Pick Up",
      "En route to delivery",
      "Arrived to delivery",
      null,
    ],
    default: null,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: [
    {
      message: {
        type: String,
      },
      time: {
        type: Date,
      },
    },
  ],
  createdDate: {
    type: Date,
    default: Date.now(),
    required: false,
  },
  created_by: {
    type: String,
    required: true,
  },
});
const LoadModel = mongoose.model("loads", LoadSchema);
module.exports = { LoadModel };
