const mongoose = require("mongoose");

const TruckSchema = new mongoose.Schema({
  createdBy: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: false,
    default: null,
  },
  type: {
    type: String,
    required: true,
    enum: ["SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT", null],
  },
  status: {
    type: String,
    required: false,
    default: "IS",
    enum: ["OL", "IS"],
  },
  createdDate: {
    type: Date,
    default: Date.now(),
    required: false,
  },
});
const TruckModel = mongoose.model("trucks", TruckSchema);
module.exports = { TruckModel };
